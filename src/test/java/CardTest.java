import no.ntnu.idatt2001.oblig3.cardgame.PlayingCard;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit test to check that PlayingCard works as expected
 */
public class CardTest {
    @Test
    public void cardTest(){
        PlayingCard card = new PlayingCard('H', 2);
        assertEquals(card.getAsString(), "H2");
    }
}
