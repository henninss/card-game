import no.ntnu.idatt2001.oblig3.cardgame.DeckOfCards;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


/**
 * Unit test to check that DeckOfCards behaves as expected
 */
public class DeckTest {
    @Test
    public void deckTest(){
        DeckOfCards deck = new DeckOfCards();
        assertEquals(5, deck.dealHand(5).size());
        assertNotEquals(deck.dealHand(5), deck.dealHand(5));
        assertEquals(deck.getDeck().size(), 52);
    }
}
